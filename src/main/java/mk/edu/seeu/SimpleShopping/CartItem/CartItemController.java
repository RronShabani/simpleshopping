package mk.edu.seeu.SimpleShopping.CartItem;

import mk.edu.seeu.SimpleShopping.ItemType.ItemType;
import mk.edu.seeu.SimpleShopping.ItemType.ItemTypeRepository;
import mk.edu.seeu.SimpleShopping.ShoppingCart.ShoppingCart;
import mk.edu.seeu.SimpleShopping.ShoppingCart.ShoppingCartRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.InputMismatchException;
import java.util.List;

@RestController
@RequestMapping("/api/items")
public class CartItemController {

    private final CartItemRepository cartItemRepository;
    private final ItemTypeRepository itemTypeRepository;
    private final ShoppingCartRepository shoppingCartRepository;

    public CartItemController(CartItemRepository cartItemRepository, ItemTypeRepository itemTypeRepository, ShoppingCartRepository shoppingCartRepository) {
        this.cartItemRepository = cartItemRepository;
        this.itemTypeRepository = itemTypeRepository;
        this.shoppingCartRepository = shoppingCartRepository;
    }

    @GetMapping
    public ResponseEntity<List<CartItem>> findAll() {
        List<CartItem> cartItems = cartItemRepository.findAll();
        return ResponseEntity.ok(cartItems);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CartItem> findById(@PathVariable(name = "id") Long id) {
        CartItem cartItem = cartItemRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        return ResponseEntity.ok(cartItem);
    }

    @PostMapping
    public ResponseEntity<CartItem> create(@RequestBody CartItem cartItem) {
        CartItem response = cartItemRepository.save(cartItem);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CartItem> update(@PathVariable(name = "id") Long id, @RequestBody CartItem cartItem) {
        if (!cartItem.getId().equals(id)) {
            throw new InputMismatchException("IDs do not match!");
        }
        if (cartItemRepository.findById(id).isEmpty()) {
            throw new EntityNotFoundException("cartItem not found!");
        }
        CartItem response = cartItemRepository.save(cartItem);
        return ResponseEntity.status(HttpStatus.CREATED).body(cartItemRepository.save(response));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable(name = "id") Long id) {
        if (cartItemRepository.findById(id).isEmpty()) {
            throw new EntityNotFoundException("cartItem not found!");
        }
        CartItem cartItem = cartItemRepository.findById(id)
                .orElseThrow(EntityNotFoundException::new);

        for (ItemType itemType : itemTypeRepository.findAllByCartItemsContaining(cartItem)) {
            itemType.getCartItems().remove(cartItem);
            itemTypeRepository.save(itemType);
        }

        for (ShoppingCart shoppingCart : shoppingCartRepository.findAllByCartItemsContaining(cartItem)) {
            shoppingCart.getCartItems().remove(cartItem);
            shoppingCartRepository.save(shoppingCart);
        }
        cartItemRepository.deleteById(id);
    }

}
