package mk.edu.seeu.SimpleShopping.CartItem;

import mk.edu.seeu.SimpleShopping.ItemType.ItemType;
import mk.edu.seeu.SimpleShopping.ShoppingCart.ShoppingCart;

import javax.persistence.*;

@Entity
public class CartItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private ItemType itemType;
    private Long amount;
    @ManyToOne
    private ShoppingCart shoppingCart;

    public CartItem() {
    }

    public Long getId() {
        return id;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }
}
