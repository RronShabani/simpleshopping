package mk.edu.seeu.SimpleShopping.ShoppingCart;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;

@Controller
@RequestMapping("/carts")
public class ShoppingCartViewController {
    private final ShoppingCartRepository shoppingCartRepository;

    public ShoppingCartViewController(ShoppingCartRepository shoppingCartRepository) {
        this.shoppingCartRepository = shoppingCartRepository;
    }

    @GetMapping
    public String findAll(Model model) {
        model.addAttribute("shoppingCarts", shoppingCartRepository.findAll());
        return "shoppingCarts";
    }

    @GetMapping("/{id}")
    public String findById(@PathVariable(name = "id") Long id, Model model) {
        model.addAttribute("shoppingCart",
                shoppingCartRepository.findById(id).orElseThrow(EntityNotFoundException::new));
        return "shoppingCart";
    }

    @PostMapping
    public String save(@ModelAttribute ShoppingCart shoppingCart) {
        System.out.println(shoppingCart.getId());
        shoppingCartRepository.save(shoppingCart);
        return "redirect:/carts";
    }

    @GetMapping("/create")
    public String create(Model model) {
        ShoppingCart shoppingCart = new ShoppingCart();
        model.addAttribute("shoppingCart", shoppingCart);
        model.addAttribute("action", "/carts");
        return "shoppingCart_create";
    }

    @GetMapping("/{id}/edit")
    public String edit(@PathVariable Long id, Model model) {
        ShoppingCart shoppingCart = shoppingCartRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        model.addAttribute("shoppingCart", shoppingCart);
        model.addAttribute("action", "/carts/" + id + "/update");
        return "shoppingCart_create";
    }

    @PostMapping("/{id}/update")
    public String update(@ModelAttribute ShoppingCart shoppingCart, @PathVariable long id) {
        ShoppingCart cart = shoppingCartRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        shoppingCartRepository.save(cart);
        return "redirect:/carts";
    }
}
