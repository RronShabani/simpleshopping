package mk.edu.seeu.SimpleShopping.ShoppingCart;

import mk.edu.seeu.SimpleShopping.CartItem.CartItem;
import mk.edu.seeu.SimpleShopping.CartItem.CartItemRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.InputMismatchException;
import java.util.List;

@RestController
@RequestMapping("/api/carts")
public class ShoppingCartController {
    private final ShoppingCartRepository shoppingCartRepository;
    private final CartItemRepository cartItemRepository;

    public ShoppingCartController(ShoppingCartRepository shoppingCartRepository, CartItemRepository cartItemRepository) {
        this.shoppingCartRepository = shoppingCartRepository;
        this.cartItemRepository = cartItemRepository;
    }

    @GetMapping
    public ResponseEntity<List<ShoppingCart>> findAll() {
        List<ShoppingCart> shoppingCarts = shoppingCartRepository.findAll();
        return ResponseEntity.ok(shoppingCarts);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ShoppingCart> findById(@PathVariable(name = "id") Long id) {
        ShoppingCart shoppingCart = shoppingCartRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        return ResponseEntity.ok(shoppingCart);
    }

    @PostMapping
    public ResponseEntity<ShoppingCart> create(@RequestBody ShoppingCart shoppingCart) {
        ShoppingCart response = shoppingCartRepository.save(shoppingCart);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ShoppingCart> update(@PathVariable(name = "id") Long id, @RequestBody ShoppingCart shoppingCart) {
        if (!shoppingCart.getId().equals(id)) {
            throw new InputMismatchException("IDs do not match!");
        }
        if (shoppingCartRepository.findById(id).isEmpty()) {
            throw new EntityNotFoundException("shoppingCart not found!");
        }
        ShoppingCart response = shoppingCartRepository.save(shoppingCart);
        return ResponseEntity.status(HttpStatus.CREATED).body(shoppingCartRepository.save(response));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable(name = "id") Long id) {
        if (shoppingCartRepository.findById(id).isEmpty()) {
            throw new EntityNotFoundException("cartItem not found!");
        }
        ShoppingCart shoppingCart = shoppingCartRepository.findById(id).orElseThrow(EntityNotFoundException::new);

        for (CartItem cartItem : cartItemRepository.findAllByShoppingCart(shoppingCart)) {
            cartItem.setShoppingCart(null);
            cartItemRepository.save(cartItem);
        }
        shoppingCartRepository.deleteById(id);
    }
}
