package mk.edu.seeu.SimpleShopping.ShoppingCart;

import mk.edu.seeu.SimpleShopping.CartItem.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShoppingCartRepository extends JpaRepository<ShoppingCart, Long>, PagingAndSortingRepository<ShoppingCart, Long> {
    List<ShoppingCart> findAllByCartItemsContaining(CartItem cartItem);
}
