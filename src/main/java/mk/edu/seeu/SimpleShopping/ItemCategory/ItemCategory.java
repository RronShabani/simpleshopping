package mk.edu.seeu.SimpleShopping.ItemCategory;

import com.fasterxml.jackson.annotation.JsonBackReference;
import mk.edu.seeu.SimpleShopping.ItemType.ItemType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class ItemCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
//    @ManyToMany(mappedBy = "categories", cascade = CascadeType.ALL)
    @ManyToMany
    @JsonBackReference
    private List<ItemType> itemTypes = new ArrayList<>();

    public ItemCategory() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ItemType> getItemTypes() {
        return itemTypes;
    }

    public void setItemTypes(List<ItemType> itemTypes) {
        this.itemTypes = itemTypes;
    }
}
