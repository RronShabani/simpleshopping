package mk.edu.seeu.SimpleShopping.ItemCategory;

import mk.edu.seeu.SimpleShopping.ItemType.ItemType;
import mk.edu.seeu.SimpleShopping.ItemType.ItemTypeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.InputMismatchException;
import java.util.List;

@RestController
@RequestMapping("/api/categories")
public class ItemCategoryController {


    private final ItemCategoryRepository itemCategoryRepository;
    private final ItemTypeRepository itemTypeRepository;

    public ItemCategoryController(ItemCategoryRepository itemCategoryRepository, ItemTypeRepository itemTypeRepository) {
        this.itemCategoryRepository = itemCategoryRepository;
        this.itemTypeRepository = itemTypeRepository;
    }

    @GetMapping
    public ResponseEntity<List<ItemCategory>> findAll() {
        List<ItemCategory> itemCategories = itemCategoryRepository.findAll();
        return ResponseEntity.ok(itemCategories);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ItemCategory> findById(@PathVariable(name = "id") Long id) {
        ItemCategory itemCategory = itemCategoryRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        return ResponseEntity.ok(itemCategory);
    }

    @PostMapping
    public ResponseEntity<ItemCategory> create(@RequestBody ItemCategory itemCategory) {
        ItemCategory response = itemCategoryRepository.save(itemCategory);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ItemCategory> update(@PathVariable(name = "id") Long id, @RequestBody ItemCategory itemCategory) {
        if (!itemCategory.getId().equals(id)) {
            throw new InputMismatchException("IDs do not match!");
        }
        if (itemCategoryRepository.findById(id).isEmpty()) {
            throw new EntityNotFoundException("itemCategory not found!");
        }
        ItemCategory response = itemCategoryRepository.save(itemCategory);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable(name = "id") Long id) {
        if (itemCategoryRepository.findById(id).isEmpty()) {
            throw new EntityNotFoundException("itemCategory not found!");
        }
        ItemCategory category = itemCategoryRepository.findById(id)
                .orElseThrow(EntityNotFoundException::new);
        for(ItemType itemType: itemTypeRepository.findAllByCategoriesContains(category)){
            itemType.getCategories().remove(category);
            itemTypeRepository.save(itemType);
        }
        itemCategoryRepository.deleteById(id);
    }
}
