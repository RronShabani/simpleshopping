package mk.edu.seeu.SimpleShopping;

import mk.edu.seeu.SimpleShopping.CartItem.CartItem;
import mk.edu.seeu.SimpleShopping.CartItem.CartItemRepository;
import mk.edu.seeu.SimpleShopping.ItemCategory.ItemCategory;
import mk.edu.seeu.SimpleShopping.ItemCategory.ItemCategoryRepository;
import mk.edu.seeu.SimpleShopping.ItemType.ItemType;
import mk.edu.seeu.SimpleShopping.ItemType.ItemTypeRepository;
import mk.edu.seeu.SimpleShopping.ShoppingCart.ShoppingCart;
import mk.edu.seeu.SimpleShopping.ShoppingCart.ShoppingCartRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class SimpleShoppingApplication implements CommandLineRunner {

	private final ShoppingCartRepository shoppingCartRepository;
	private final CartItemRepository cartItemRepository;
	private final ItemTypeRepository itemTypeRepository;
	private final ItemCategoryRepository itemCategoryRepository;


	public SimpleShoppingApplication(ShoppingCartRepository shoppingCartRepository, CartItemRepository cartItemRepository, ItemTypeRepository itemTypeRepository, ItemCategoryRepository itemCategoryRepository) {
		this.shoppingCartRepository = shoppingCartRepository;
		this.cartItemRepository = cartItemRepository;
		this.itemTypeRepository = itemTypeRepository;
		this.itemCategoryRepository = itemCategoryRepository;
	}

	public static void main(String[] args) {
		SpringApplication.run(SimpleShoppingApplication.class, args);
	}

	@Override
	public void run(String... args){
		// Initializing a non-empty database for testing purposes
		// Dropping the tables before starting is encouraged :)

		ItemCategory itemCategory = new ItemCategory();
		itemCategory.setName("Food");
		itemCategoryRepository.save(itemCategory);

		ItemCategory itemCategory2 = new ItemCategory();
		itemCategory2.setName("Drink");
		itemCategoryRepository.save(itemCategory2);

		ItemCategory itemCategory3 = new ItemCategory();
		itemCategory3.setName("Fruit");
		itemCategoryRepository.save(itemCategory3);

		ItemType itemType = new ItemType();
		itemType.setName("Banana");
		itemType.setCategories(Arrays.asList(itemCategory,itemCategory3));
		itemTypeRepository.save(itemType);

		ItemType itemType2 = new ItemType();
		itemType2.setName("Cola");
		itemType2.setCategories(Arrays.asList(itemCategory2));
		itemTypeRepository.save(itemType2);

		ItemType itemType3 = new ItemType();
		itemType3.setName("Snickers");
		itemType3.setCategories(Arrays.asList(itemCategory3));
		itemTypeRepository.save(itemType3);

		CartItem cartItem = new CartItem();
		cartItem.setItemType(itemType);
		cartItem.setAmount(2L);
		cartItemRepository.save(cartItem);

		CartItem cartItem2 = new CartItem();
		cartItem2.setItemType(itemType2);
		cartItem2.setAmount(2L);
		cartItemRepository.save(cartItem2);

		CartItem cartItem3 = new CartItem();
		cartItem3.setItemType(itemType3);
		cartItem3.setAmount(2L);
		cartItemRepository.save(cartItem3);

		CartItem cartItem4 = new CartItem();
		cartItem4.setItemType(itemType);
		cartItem4.setAmount(2L);
		cartItemRepository.save(cartItem4);

		CartItem cartItem5 = new CartItem();
		cartItem5.setItemType(itemType2);
		cartItem5.setAmount(5L);
		cartItemRepository.save(cartItem5);

		CartItem cartItem6 = new CartItem();
		cartItem6.setItemType(itemType3);
		cartItem6.setAmount(2L);
		cartItemRepository.save(cartItem6);

		ShoppingCart shoppingCart = new ShoppingCart();
		shoppingCart.setCartItems(Arrays.asList(cartItem,cartItem2, cartItem3));
		shoppingCartRepository.save(shoppingCart);

		ShoppingCart shoppingCart2 = new ShoppingCart();
		shoppingCart2.setCartItems(Arrays.asList(cartItem4,cartItem5));
		shoppingCartRepository.save(shoppingCart2);

		ShoppingCart shoppingCart3 = new ShoppingCart();
		shoppingCart3.setCartItems(Arrays.asList(cartItem6));
		shoppingCartRepository.save(shoppingCart3);
	}

}
