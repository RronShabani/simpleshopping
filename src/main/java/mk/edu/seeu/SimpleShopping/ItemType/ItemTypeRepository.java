package mk.edu.seeu.SimpleShopping.ItemType;

import mk.edu.seeu.SimpleShopping.CartItem.CartItem;
import mk.edu.seeu.SimpleShopping.ItemCategory.ItemCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemTypeRepository extends JpaRepository<ItemType, Long>, PagingAndSortingRepository<ItemType, Long> {
    List<ItemType> findAllByName(String name);
    List<ItemType> findAllByCategoriesContains(ItemCategory category);
    List<ItemType> findAllByCartItemsContaining(CartItem cartItem);
}
