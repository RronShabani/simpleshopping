package mk.edu.seeu.SimpleShopping.ItemType;

import mk.edu.seeu.SimpleShopping.CartItem.CartItem;
import mk.edu.seeu.SimpleShopping.CartItem.CartItemRepository;
import mk.edu.seeu.SimpleShopping.ItemCategory.ItemCategory;
import mk.edu.seeu.SimpleShopping.ItemCategory.ItemCategoryRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.InputMismatchException;
import java.util.List;

@RestController
@RequestMapping("/api/types")
public class ItemTypeController {

    public final ItemTypeRepository itemTypeRepository;
    public final ItemCategoryRepository itemCategoryRepository;
    public final CartItemRepository cartItemRepository;

    public ItemTypeController(ItemTypeRepository itemTypeRepository, ItemCategoryRepository itemCategoryRepository, CartItemRepository cartItemRepository) {
        this.itemTypeRepository = itemTypeRepository;
        this.itemCategoryRepository = itemCategoryRepository;
        this.cartItemRepository = cartItemRepository;
    }

    @GetMapping
    public ResponseEntity<List<ItemType>> findAll() {
        List<ItemType> itemTypes = itemTypeRepository.findAll();
        return ResponseEntity.ok(itemTypes);
    }

    @GetMapping("/{name}")
    public ResponseEntity<List<ItemType>> findAllByName(@PathVariable String name) {
        List<ItemType> itemTypes = itemTypeRepository.findAllByName(name);
        return ResponseEntity.ok(itemTypes);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ItemType> findById(@PathVariable(name = "id") Long id) {
        ItemType itemType = itemTypeRepository.findById(id)
                .orElseThrow(EntityNotFoundException::new);
        return ResponseEntity.ok(itemType);
    }

    @PostMapping
    public ResponseEntity<ItemType> create(@RequestBody ItemType itemType) {
        ItemType response = itemTypeRepository.save(itemType);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ItemType> update(@PathVariable(name = "id") Long id, @RequestBody ItemType itemType) {
        if (!itemType.getId().equals(id)) {
            throw new InputMismatchException("IDs do not match!");
        }
        if (itemTypeRepository.findById(id).isEmpty()) {
            throw new EntityNotFoundException("itemType not found!");
        }
        ItemType response = itemTypeRepository.save(itemType);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable(name = "id") Long id) {
        if (itemTypeRepository.findById(id).isEmpty()) {
            throw new EntityNotFoundException("itemType not found!");
        }
        ItemType itemType = itemTypeRepository.findById(id)
                .orElseThrow(EntityNotFoundException::new);

        for (ItemCategory category : itemCategoryRepository.findAllByItemTypesContains(itemType)) {
            category.getItemTypes().remove(itemType);
            itemCategoryRepository.save(category);
        }

        for (CartItem cartItem : cartItemRepository.findAllByItemType(itemType)){
            cartItem.setItemType(null);
            cartItemRepository.save(cartItem);
        }

        itemTypeRepository.deleteById(id);
    }
}
